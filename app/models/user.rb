class User < ActiveRecord::Base

  has_many :events
  has_many :calendars
  has_one :color

  def self.create_with_omniauth(auth)
    create! do |user|
      user.provider      = auth.fetch('provider')
      user.uid           = auth.fetch('uid')
      user.access_token  = auth.fetch('credentials').fetch('token')
      user.expires_at    = Time.at auth.fetch('credentials').fetch('expires_at')
      if auth['info']
         user.name = auth['info']['name'] || ''
      end
    end
  end

  def developer?
    id.in? [1, 2, 4, 5]
  end

end
