class CalendarCheckboxes

  SELECTOR = '[data-update-calendars]'

  constructor: ->
    $(SELECTOR).on 'change', @onCalendarCheckboxClick

  onCalendarCheckboxClick: (event) ->
    $this = $(event.currentTarget)
    $.ajax(
      url: $this.data('url')
      method: 'POST'
      data:
        calendar_id: $this.data('calendar-id')
        checked: $this.is(':checked')
    )

$ ->
  new CalendarCheckboxes()