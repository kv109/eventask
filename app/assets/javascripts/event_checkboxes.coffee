class EventCheckboxes
  SELECTOR = '[data-update-event-status]'

  constructor: ->
    $(SELECTOR).on 'click', @onClick

  onClick: (event) ->
    $this = $(event.currentTarget)

    $.ajax(
      url: $this.data('update-event-url')
      method: 'POST'
      data:
        uid: $this.data('event-uid')
        checked: $this.is(':checked')
      success: ->
        document.location.reload()
    )

$ ->
  new EventCheckboxes()