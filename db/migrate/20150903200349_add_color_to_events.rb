class AddColorToEvents < ActiveRecord::Migration
  def change
    add_column :events, :color, :string, default: '#ffffff'
  end
end
