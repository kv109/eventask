class Color < ActiveRecord::Base

  belongs_to :user

  def merge_google_colors!(google_colors)
    merge_google_colors(google_colors)
    save!
  end

  def merge_google_colors(google_colors)
    self.colors = google_colors
  end

end
