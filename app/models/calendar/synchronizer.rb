class Calendar::Synchronizer

  def initialize(user)
    @user = user  
  end

  def synchronize!
    remove_deleted_calendars!
    merge_google_calendars!
  end
  
  private

  def merge_google_calendars!
    all_user_calendars.each do |google_calendar|
      find_or_initialize_calendar(google_calendar.fetch('id')).tap do |calendar|
        calendar.merge_google_calendar!(google_calendar)
      end
    end
  end

  def remove_deleted_calendars!
    @user.calendars.each do |calendar|
      calendar.destroy unless calendar.uid.in? google_calendars_uids
    end
  end

  def find_or_initialize_calendar(google_calendar_id)
    @user.calendars.find_by_uid(google_calendar_id) || @user.calendars.new(uid: google_calendar_id)
  end

  def all_user_calendars
    google_calendar.all_calendars
  end

  def google_calendars_uids
    @google_calendars_uids ||= all_user_calendars.map do |google_calendar|
      google_calendar.fetch('id')
    end
  end

  def google_calendar
    @calendar ||= GoogleCalendar.new(@user)
  end

end
