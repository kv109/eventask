class Color::Synchronizer

  def initialize(user)
    @user = user  
  end

  def synchronize!
    @user.color.merge_google_colors!(google_calendar.colors)
  end
  
  private

  def google_calendar
    @calendar ||= GoogleCalendar.new(@user)
  end

end
