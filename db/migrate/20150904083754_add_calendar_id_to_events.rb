class AddCalendarIdToEvents < ActiveRecord::Migration
  def change
    add_column :events, :calendar_id, :integer
    add_index :events, :calendar_id
  end
end
