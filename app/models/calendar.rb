class Calendar < ActiveRecord::Base

  belongs_to :user

  scope :ordered, -> { order(:summary) }
  scope :to_synchronize, -> { where(synchronize: true) }

  def merge_google_calendar!(google_calendar)
    merge_google_calendar(google_calendar)
    save!
  end

  def merge_google_calendar(google_calendar)
    self.uid = google_calendar.fetch('id')
    self.summary = google_calendar.fetch('summary')
    self.background_color = google_calendar.fetch('backgroundColor')
  end

end
