class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.string :uid
      t.integer :user_id
      t.string :summary
      t.boolean :synchronize, default: true
    end
  end
end
