class FillEventsColor < ActiveRecord::Migration
  def up
    Event.where(calendar_id: nil).find_each do |event|
      first_calendar = event.user.calendars.first
      event.update_attribute(:calendar_id, first_calendar.id) if first_calendar
    end
    Event.find_each do |event|
      color = event.default_color rescue '#ffffff'
      event.update_attribute(:color, color)
    end
  end

  def down

  end
end
