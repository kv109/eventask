class CalendarsController < ApplicationController

  before_action :get_calendar, only: [:update]

  def index
    @calendars = current_user.calendars.ordered
  end

  def update
    @calendar.update_attribute(:synchronize, params.fetch(:checked) == 'true')
    head :ok
  end

  private

  def get_calendar
    @calendar = current_user.calendars.find_by_uid(params.fetch(:calendar_id))
  end

end
