class EventsController < ApplicationController

  before_action :get_event, only: [:update, :archive, :restore]

  def index
    events = current_user.events.visible
    @events_to_do = events.not_done
    @events_done  = events.done
  end

  def archived
    @events = current_user.events.archived
  end

  def update
    checked = params.fetch(:checked) == 'true'
    @event.update_attribute(:task_status, checked ? Event::TASK_STATUSES.fetch(:done) : nil)
    head :ok
  end

  def archive
    @event.update_attribute(:archived, true)
    redirect_to root_path, notice: 'Event archived'
  end

  def restore
    @event.update_attributes(archived: false, task_status: params[:task_status])
    redirect_to root_path, notice: 'Event restored'
  end

  def refresh
    Event::Synchronizer.new(current_user).synchronize!
    redirect_to root_path, notice: 'Events synchronized with Google Calendar'
  rescue Google::APIClient::AuthorizationError
    redirect_to signout_path
  end

  private

  def get_event
    @event = Event.find_by_uid(params.fetch(:uid))
  end

end
