class CreateColorsForUsers < ActiveRecord::Migration
  def up
    User.find_each do |user|
      user.build_color.save!
    end
  end
end
