class Event::GoogleEventAdapter

  def initialize(event, google_event)
    @event = event
    @google_event = google_event
  end

  def summary
    @google_event.fetch('summary')
  end

  def start_datetime
    start['date'] || start.fetch('dateTime')
  end

  def start
    @google_event.fetch('start')
  end

  def html_link
    @google_event.fetch('htmlLink')
  end

  def color
    custom_color || @event.default_color
  end

  private

  def color_id
    @google_event['colorId']
  end

  def custom_color
    events_colors[color_id].try(:fetch, 'background')
  end

  def events_colors
    @events_colors ||= @event.user.color.colors.fetch('event')
  end

end
