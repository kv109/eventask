class Event < ActiveRecord::Base

  belongs_to :user
  belongs_to :calendar

  TASK_STATUSES = {
    done: 'done',
    not_done: nil
  }

  ATTRIBUTES_TO_SYNCHRONIZE = %w(summary start_datetime html_link color)

  validates :task_status, inclusion: { in: TASK_STATUSES.values }, allow_nil: true

  scope :not_done, -> { where(task_status: TASK_STATUSES.fetch(:not_done)) }
  scope :done, -> { where(task_status: TASK_STATUSES.fetch(:done)) }
  scope :visible, -> { where(archived: false).order(:start_datetime) }
  scope :archived, -> { where(archived: true).order(:start_datetime) }
  scope :future, -> { where('start_datetime > ?', DateTime.now) }
  scope :past, -> { where('start_datetime <= ? ', DateTime.now) }


  def done?
    task_status == TASK_STATUSES.fetch(:done)
  end

  def merge_google_event(google_event, calendar_uid)
    google_event = Event::GoogleEventAdapter.new(self, google_event)

    self.calendar_id = user.calendars.find_by_uid(calendar_uid).id

    ATTRIBUTES_TO_SYNCHRONIZE.each do |attribute|
      setter = "#{attribute}="
      value = google_event.send(attribute)
      self.send(setter, value)
    end
  end

  def merge_google_event!(google_event, calendar_uid)
    merge_google_event(google_event, calendar_uid)
    save!
  end

  def default_color
    calendar.background_color
  end
end
