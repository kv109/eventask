class CreateColors < ActiveRecord::Migration
  def change
    create_table :colors do |t|
      t.integer :user_id
      t.json :colors
    end

    add_index :colors, :user_id, unique: true
  end
end
