# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150904105023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "calendars", force: :cascade do |t|
    t.string  "uid"
    t.integer "user_id"
    t.string  "summary"
    t.boolean "synchronize",      default: true
    t.string  "background_color", default: "#ffffff"
  end

  create_table "colors", force: :cascade do |t|
    t.integer "user_id"
    t.json    "colors"
  end

  add_index "colors", ["user_id"], name: "index_colors_on_user_id", unique: true, using: :btree

  create_table "events", force: :cascade do |t|
    t.integer  "user_id",                            null: false
    t.string   "uid"
    t.string   "summary"
    t.string   "task_status"
    t.string   "html_link"
    t.datetime "start_datetime"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "archived",       default: false
    t.string   "color",          default: "#ffffff"
    t.integer  "calendar_id"
  end

  add_index "events", ["archived"], name: "index_events_on_archived", using: :btree
  add_index "events", ["calendar_id"], name: "index_events_on_calendar_id", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "provider"
    t.string   "uid"
    t.string   "access_token"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
