class CreateEvents < ActiveRecord::Migration
  def up
    create_table :events do |t|
      t.integer :user_id, null: false
      t.string :uid
      t.string :summary
      t.string :task_status
      t.string :html_link
      t.datetime :start_datetime

      t.timestamps null: false
    end

    add_index :events, :user_id
  end

  def down
    drop_table :events
  end
end
