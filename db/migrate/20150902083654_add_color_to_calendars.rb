class AddColorToCalendars < ActiveRecord::Migration
  def change
    add_column :calendars, :background_color, :string, default: '#ffffff'
  end
end
