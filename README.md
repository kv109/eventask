# README #

### PostgreSQL
I had to set dir directly:
`gem install pg -v '0.17.1' -- --with-pg-config=/Library/PostgreSQL/9.4/bin/pg_config`

### OpenSSL problem
If you get
`OpenSSL::SSL::SSLError: SSL_connect returned=1 errno=0 state=SSLv3
read server certificate B: certificate verify failed`

then
`rvm install 2.1.6  --with-openssl-dir=/usr/local/etc/openssl/`