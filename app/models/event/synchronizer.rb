class Event::Synchronizer

  SYNCHRONIZE_FROM = 2.days
  SYNCHRONIZE_TO = 7.days

  def initialize(user)
    @user = user  
  end

  def synchronize!
    remove_deleted_events!
    merge_google_events!
  end

  private

  def merge_google_events!
    google_events_grouped_by_calendar_uid.each do |calendar_uid, google_events|
      google_events.each do |google_event|
        find_or_initialize_event(google_event.fetch('id')).tap do |event|
          event.merge_google_event!(google_event, calendar_uid)
        end
      end
    end
  end

  def remove_deleted_events!
    events.not_done.each do |event|
      event.destroy unless event.uid.in? google_events_uids
    end
  end

  def find_or_initialize_event(google_event_id)
    events.find_by_uid(google_event_id) || events.new(uid: google_event_id)
  end

  def events
    @events ||= @user.events
  end

  def google_events_uids
    @google_calendar_events_uids ||= google_events_grouped_by_calendar_uid.values.flatten.map do |google_calendar_event|
      google_calendar_event.fetch('id')
    end
  end

  def google_events_grouped_by_calendar_uid
    @google_calendar_events ||= google_calendar.events_grouped_by_calendars_uids(
      calendars_to_synchronize_uids,
      from: SYNCHRONIZE_FROM, to: SYNCHRONIZE_TO
    )
  end

  def calendars_to_synchronize_uids
    @user.calendars.to_synchronize.pluck(:uid)
  end

  def google_calendar
    @calendar ||= GoogleCalendar.new(@user)
  end

end