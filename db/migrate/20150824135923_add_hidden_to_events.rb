class AddHiddenToEvents < ActiveRecord::Migration
  def change
    add_column :events, :archived, :boolean, default: false
    add_index :events, :archived
  end
end
