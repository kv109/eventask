require 'google/api_client'

class GoogleCalendar

  EVENTS_FROM  = 2.days
  EVENTS_TO    = 7.days
  EVENTS_ORDER = 'startTime'

  def initialize(user)
    @user = user
  end

  def events_by_calendar_uid(calendar_uid, options = {})
    from = options[:from] || EVENTS_FROM
    to   = options[:to] || EVENTS_TO

    get_json(
      'events.list',
      {
        calendarId: calendar_uid,
        singleEvents: true,
        orderBy: EVENTS_ORDER,
        timeMin: (Time.now - from).iso8601,
        timeMax: (Time.now + to).iso8601,
      }
    ).fetch('items')

  rescue Google::APIClient::ClientError => e
    case e.message
      when CalendarsSynchronizationRequiredError::MESSAGE
        raise CalendarsSynchronizationRequiredError
      when GoogleCalendarError::INVALID_CREDENTIALS_MESSAGE
        raise e
      else
        raise e
    end
  end

  def events_grouped_by_calendars_uids(calendars_uids, options = {})
    {}.tap do |hash|
      calendars_uids.each do |calendar_uid|
        hash[calendar_uid] = events_by_calendar_uid(calendar_uid, options)
      end
    end
  rescue CalendarsSynchronizationRequiredError
    if @calendars_already_synchronized
      raise CalendarsSynchronizationLoopError
    else
      Calendar::Synchronizer.new(@user).synchronize!
      @calendars_already_synchronized = true
      events_grouped_by_calendars_uids(calendars_uids, options)
    end
  end

  def all_calendars
    @all_calendars ||= get_json('calendar_list.list').fetch('items')
  end

  def colors
    @colors ||= get_json('colors.get')
  end

  private

  def get_json(method, parameters = {})
    response_body = call(method, parameters).body
    JSON.parse(response_body)
  end

  def call(method, parameters)
    api_client.execute!(api_method: eval("calendar.#{method}"), parameters: parameters)
  end

  def api_client
    @api_client ||= Google::APIClient.new.tap do |api_client|
      api_client.authorization.access_token = @user.access_token
    end
  end

  def calendar
    api_client.discovered_api('calendar', 'v3')
  end

  class GoogleCalendarError < StandardError
    INVALID_CREDENTIALS_MESSAGE = 'Invalid Credentials'
  end

  class CalendarsSynchronizationRequiredError < GoogleCalendarError
    MESSAGE = 'Not Found'
  end

  class CalendarsSynchronizationLoopError < GoogleCalendarError; end

end
