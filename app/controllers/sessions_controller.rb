class SessionsController < ApplicationController

  skip_before_filter :authenticate_user!, only: [:new, :create]

  def new
    redirect_to '/auth/google_oauth2'
  end

  def create
    auth = request.env["omniauth.auth"]
    user = User.where(uid: auth['uid'].to_s).first

    if user
      user.update_attribute(:access_token, auth.fetch('credentials').fetch('token'))
      Calendar::Synchronizer.new(user).synchronize!
      Color::Synchronizer.new(user).synchronize!
    else
      user = User.create_with_omniauth(auth)
    end

    Event::Synchronizer.new(user).synchronize!
    reset_session
    session[:user_id] = user.id
    redirect_to root_url, :notice => 'Signed in!'
  end

  def destroy
    reset_session
    redirect_to root_url, :notice => 'Signed out!'
  end

  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end

end
